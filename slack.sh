#!/bin/bash
#
# Script to send a Slack message trigerred by certain event

webhookurl="$SLACK_TOKEN"
channel="daily-updates"
BRL=$(cat BRL)
WISE=$(cat WISE)
ETH=$(cat ETH)
TEMP=$(cat TEMP)
HOME_TO_WORK=$(cat HOME_TO_WORK)
NEWS=$(cat NEWS)
PROPERTY=$(cat PROPERTY)
colour="#36a64f" # Green

short_msg (){
curl -X POST --data-urlencode 'payload={"username":"Daily Updates", "icon_emoji": "newspaper", "channel": "'${channel}'",
"attachments":[
      {
          "color": "'${colour}'",
          "fields": [
              {
                  "title": "Exchange rate FIAT:",
                  "value": "1 EUR = '"${BRL}"' BRL"
              },
              {
                  "title": "Exchange rate Crypto:",
                  "value": "1 USD = '"${ETH}"' ETH"
              },
              {
                  "title": "Weather forecast Dublin, IE:",
                  "value": "'"${TEMP}"'"
              }
          ],
      }
   ]
}' \
  ${webhookurl}
}

short_msg

